import React, { useEffect, useState, Component } from 'react';
import { Link, Route } from "react-router-dom";
import funql from 'funql-api/client'
const fql = funql('http://localhost:3001')
import { Button, Text } from "@blueprintjs/core";

console.log(process.env.PORT)

class Blog extends Component {
    handleClick(){
        fql('hello').then(console.info)
    }

    render() {
        return (
            <div>
                <h1>BLOG</h1>
                <p>
                    You can change the text in the input below. Hover to see full text. If the text is long enough, then the content will overflow. This is done by setting ellipsize to true.
        </p>
                <Text ellipsize={false}>
                    You can change the text in the input below. Hover to see full text. If the text is long enough, then the content will overflow. This is done by setting ellipsize to true.
        </Text>
                <Button intent="success" large={true} outlined={true.toString()}
                    onClick={this.handleClick}
                    text="button content" />
                <ul>
                    <li>
                        <Link to="/blog/home">Home</Link>
                    </li>
                    <li>
                        <Link to="/blog/about">About</Link>
                    </li>
                </ul>
                <hr />
                <Route exact path="/blog/home" component={Home} />
                <Route path="/blog/about" component={About} />
            </div>
        );
    }
}


function Home() {
    return (
        <div>
            <h2>Home</h2>
            This is an example of browser-side routing + SSR.
        <br /><br />
            SSR is enabled:
        see <HtmlSource pathname={'/'} /> to see this content rendered to HTML.
      </div>
    );
}

function About() {
    return (
        <div>
            <h2>About</h2>
            This is content for the <code>/about</code> page.
        <br /><br />
            This content is rendered to HTML as well, see <HtmlSource pathname={'/about'} />.
      </div>
    );
}

function HtmlSource({ pathname }) {
    return (
        <code>{"view-source:http://localhost:3000" + pathname}</code>
    );
}
export default Blog