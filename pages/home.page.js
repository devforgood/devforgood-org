// pages/hello.page.js

import React, {useState} from 'react';

export default {
  route: '/',
  addInitialProps,
  view: Hello,
  title,
  renderToHtml: true,
  renderToDom:true,
  renderHtmlAtBuildTime:false
};

async function addInitialProps({}) {

  
  return {};
}

function Hello({ }) {
  

  return (
    <div>
      HOME
    </div>
  );
}

function title({}) {
  return 'HOME ';
}
