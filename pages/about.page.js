// pages/hello.page.js

import React, {useState} from 'react';

export default {
  route: '/about',
  addInitialProps,
  view: Hello,
  title,
  renderToHtml: true,
  renderToDom:true,
  renderHtmlAtBuildTime:true
};

async function addInitialProps({}) {

  
  return {};
}

function Hello({ }) {
  

  return (
    <div>
      ABOUT
    </div>
  );
}

function title({}) {
  return 'ABOUT ';
}
